//
//  ImageProrperties.swift
//  downloadImage
//
//  Created by dmitrii on 16.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation
import UIKit

struct ImageProrperties {
    
    let key: String
    let data: Data
    
    init?(withImage image: UIImage, forKey key: String) {
        self.key = key
        guard let data = image.pngData() else { return nil }
        self.data = data
    }
}
