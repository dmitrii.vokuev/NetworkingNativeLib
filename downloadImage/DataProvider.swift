//
//  DataProvider.swift
//  downloadImage
//
//  Created by dmitrii on 17.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import UIKit

class DataProvider: NSObject {

    private var downloadTask: URLSessionDownloadTask!
    var fileLocation: ((URL)->())?
    var onProgress: ((Double)->())?
    
    private lazy var bgSession: URLSession = {
        let config = URLSessionConfiguration.background(withIdentifier: "dmitrii.dev.downloadImage")
        config.isDiscretionary = true     // запуск задачи в оптимальное время (по умолчанию false)
        config.timeoutIntervalForResource = 300 // время ожидания сети в секундах
        config.waitsForConnectivity = true  // ожидание подключения к сети (по умолчанию true)
        config.sessionSendsLaunchEvents = true
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }()
    
    
    func startDownload() {
        
        if let url = URL(string: "https://speed.hetzner.de/100MB.bin") {
            downloadTask = bgSession.downloadTask(with: url)
            downloadTask.earliestBeginDate = Date().addingTimeInterval(3)
            downloadTask.countOfBytesClientExpectsToSend = 512
            downloadTask.countOfBytesClientExpectsToReceive = 100 * 1024 * 10124
            downloadTask.resume()
        }
    }
    
    func stopDownload() {
        downloadTask.cancel()
    }
}


extension DataProvider: URLSessionDelegate {
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        
        DispatchQueue.main.async {
            guard
                let appdDlegate = UIApplication.shared.delegate as? AppDelegate,
                let completionHandler = appdDlegate.bgSessionCompletionHandler
                else { return }
            
            appdDlegate.bgSessionCompletionHandler = nil
            completionHandler()
        }
    }
}


extension DataProvider: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("did finish downloading: \(location.absoluteString)")
        
        DispatchQueue.main.async {
            self.fileLocation?(location)
        }
    }
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
      //  guard totalBytesExpectedToWrite = NSURLSessionTransferSizeUnknown else { return }
        let progress = Double(Double(totalBytesWritten) / Double(totalBytesExpectedToWrite))
        //print("Download progress: \(progress)")
        DispatchQueue.main.async {
            self.onProgress?(progress)
        }
    }
    // восстановление соеденения с сетью
    func urlSession(_ session: URLSession, taskIsWaitingForConnectivity task: URLSessionTask) {
    // ожидание соединения, обновление интерфейса и прочее
    }
    
}
