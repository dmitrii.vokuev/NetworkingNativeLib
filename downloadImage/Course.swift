//
//  Course.swift
//  downloadImage
//
//  Created by dmitrii on 15.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation


struct Course: Decodable {
    let id: Int?
    let name: String?
    let link: String?
    let imageUrl: String?
    let numberOfLessons: Int?
    let numberOfTests: Int?
    
}
