
//
//  WebSiteDescription.swift
//  downloadImage
//
//  Created by dmitrii on 15.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation


struct WebSiteDescription: Decodable {
    let websiteDescription: String?
    let websiteName: String?
    let courses: [Course]
}
